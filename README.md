# finch_gleam

[![Package Version](https://img.shields.io/hexpm/v/finch_gleam)](https://hex.pm/packages/finch_gleam)
[![Hex Docs](https://img.shields.io/badge/hex-docs-ffaff3)](https://hexdocs.pm/finch_gleam/)

A wrapper/adapter package for the [Finch](https://hexdocs.pm/finch) HTTP client.

This library adapts the Finch functions to accept and return [gleam/http](https://hexdocs.pm/gleam_http) requests and responses.

## Installation

This package can be added to your Gleam project:

```sh
gleam add finch_gleam
```

and its documentation can be found at <https://hexdocs.pm/finch_gleam>.

## Usage

```gleam
import gleam/uri
import gleam/http
import gleam/http/request
import gleam/erlang/atom
import finch

assert Ok(url) = uri.parse("https://example.com/")
assert Ok(req) = request.from_uri(url)
let req = request.Request(..req, method: http.Post, body: "foo bar")

// Build Finch request from gleam/http request
let finch_req = finch.build(req, [])

// Run request with Finch, using Finch.Server, returns gleam/http response
assert Ok(resp) = finch.request(finch_req, atom.create_from_string("Elixir.Finch.Server"))
```
