//// Types related to OTP usage of Finch.

import gleam/uri
import gleam/dynamic.{type Dynamic}
import gleam/dict.{type Dict}
import gleam/erlang/atom.{type Atom}
import gleam/erlang/process.{type Pid}
import finch/timeout.{type Timeout}

pub type UnixSocketPath {
  Local(path: String)
}

pub type UnixSocketSpecifier {
  Http(path: UnixSocketPath)
  Https(path: UnixSocketPath)
}

pub type PoolSpecifier {
  Default
  URL(url: uri.Uri)
  UnixSocket(specifier: UnixSocketSpecifier)
}

pub type PoolProtocol {
  Http1
  Http2
}

pub type PoolOption {
  Protocol(PoolProtocol)
  Size(Int)
  Count(Int)
  MaxIdleTime(Timeout)
  ConnOpts(List(#(Atom, Dynamic)))
  PoolMaxIdleTime(Timeout)
  ConnMaxIdleTime(Timeout)
}

pub type PoolOptions =
  List(PoolOption)

pub type InstanceOption {
  Name(Atom)
  Pools(Dict(PoolSpecifier, PoolOptions))
}

pub type InstanceOptions =
  List(InstanceOption)

/// Returned by `start_link/2`.
///
/// Note that this is not the same as Gleam's standard `Result` type! You will
/// need to match this manually, it cannot be used with `try` syntax.
pub type OnStart {
  Ok(child: Pid)
  Ignore
  Error(Dynamic)
}
