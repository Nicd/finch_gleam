import finch/headers.{type Headers as ReqHeaders}

/// A block of data sent by Finch from the HTTP request stream.
pub type StreamBlock {
  Status(status: Int)
  Headers(headers: ReqHeaders)
  Data(data: BitArray)
}

/// A stream handler function
///
/// Receives a block of the stream data as the first argument and the
/// accumulator as the second, must return the accumulator.
pub type StreamFun(a) =
  fn(StreamBlock, a) -> a
