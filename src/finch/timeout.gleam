//// Wrapper of Elixir's `timeout/0` type.
////
//// See [https://hexdocs.pm/elixir/typespecs.html#built-in-types](https://hexdocs.pm/elixir/typespecs.html#built-in-types)
//// for a specification of the type.

import gleam/dynamic

/// A timeout value, either infinite or a specified amount of milliseconds.
pub type Timeout

/// Create an infinite timeout.
pub fn infinity() -> Timeout {
  dynamic.from(Infinity)
  |> dynamic.unsafe_coerce()
}

/// Create a timeout with the given milliseconds.
pub fn milliseconds(ms: Int) -> Timeout {
  dynamic.from(ms)
  |> dynamic.unsafe_coerce()
}

// Janky way to create the atom "infinity"
type InfinityAtom {
  Infinity
}
