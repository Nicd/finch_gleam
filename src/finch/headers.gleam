pub type Header =
  #(String, String)

pub type Headers =
  List(Header)
